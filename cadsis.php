<?php

    include 'regras/config.php';
    include 'regras/conexao.php';
    $sql = new conexao();

    $consulta = "SELECT * FROM `agua`";
    $sql->sql_consulta($consulta);
    $agua = '<table style="border:none; width: 100%">';
    while($resultado = $sql->resultado()){
        $agua.= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["agua_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["agua_codigo"].'&sis=0"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["agua_codigo"].'&sis=0" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $agua .= '</table>';

    $consulta = "SELECT * FROM  `estado_civil`";
    $sql->sql_consulta($consulta);
    $estado_civil = '<table style="border:none; width: 100%">';
    while($resultado =$sql->resultado()){
		$estado_civil .= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["ec_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["ec_codigo"].'&sis=1"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["ec_codigo"].'&sis=1" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $estado_civil .= '</table>';

    $consulta = "SELECT * FROM  `luz`";
    $sql->sql_consulta($consulta);
    $luz = '<table style="border:none; width: 100%">';
    while($resultado =$sql->resultado()){
        $luz .= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["luz_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["luz_codigo"].'&sis=2"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["luz_codigo"].'&sis=2" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $luz .= '</table>';

    $consulta = "SELECT * FROM  `moradia_situacao`";
    $sql->sql_consulta($consulta);
    $moradia = '<table style="border:none; width: 100%">';
    while($resultado =$sql->resultado()){
        $moradia .= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["sit_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["sit_codigo"].'&sis=3"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["sit_codigo"].'&sis=3" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $moradia .= '</table>';

    $consulta = "SELECT * FROM  `saneamento`";
    $sql->sql_consulta($consulta);
    $saneamento = '<table style="border:none; width: 100%">';
    while($resultado =$sql->resultado()){
        $saneamento .= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["esgoto_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["esgoto_codigo"].'&sis=4"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["esgoto_codigo"].'&sis=4" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $saneamento .= '</table>';

    $consulta = "SELECT * FROM  `programa`";
    $sql->sql_consulta($consulta);
    $programas = '<table style="border:none; width: 100%">';
    while($resultado =$sql->resultado()){
        $programas .= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["programa_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["programa_codigo"].'&sis=5"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["programa_codigo"].'&sis=5" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $programas .= '</table>';

    $consulta = "SELECT * FROM  `bairros`";
    $sql->sql_consulta($consulta);
    $bairros = '<table style="border:none; width: 100%">';
    while($resultado =$sql->resultado()){
        $bairros .= '<tr style="border:none;"><td width="80%" style="border:none;">'.$resultado["bairro_desc"].'</td><td style="border:none;"><a href="altcadsis.php?codigo='.$resultado["bairro_codigo"].'&sis=6"><img src="imagem/lapis.jpg"></a></td><td style="border:none;"><a href="regras/apaga_cadsis.php?codigo='.$resultado["bairro_codigo"].'&sis=6" onclick="return confirma()"><img src="imagem/x.jpg"></a></td></tr>';
    }
    $bairros .= '</table>';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
        <script>
            function valida(form){
                if(form.desc.value==''){
                    alert("Você deve digitar a DESCRIÇÃO!");
                    form.desc.focus();
                    return false;
                }
                return true;
            }

            function confirma(){
                if(confirm("Você tem certeza que deseja apagar esse registro? Todos os cadastros que possuem esse registro serão setados como 'vazio'."))
                    return true;
                else
                    return false;
            }
        </script>
    </head>
    <body>
        <?php
        echo $menu;
        ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>CADASTROS DO SISTEMA</H1>
                <div class="cadastros-container">
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Saneamento</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"></td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $saneamento; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`saneamento` (`esgoto_codigo`,`esgoto_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Estado Civil</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"></td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $estado_civil; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`estado_civil` (`ec_codigo`,`ec_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Programas</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"></td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $programas; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`programa` (`programa_codigo`,`programa_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Bairros</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"></td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $bairros; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`bairros` (`bairro_codigo`,`bairro_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Luz</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"></td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $luz; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`luz` (`luz_codigo`,`luz_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Água</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"> </td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $agua; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`agua` (`agua_codigo`,`agua_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                    <div>
                        <table align="center">
                            <form action="regras/cadastrarsis.php" method="POST" onsubmit="return valida(this)">
                                <tr>
                                    <td colspan="3" id="cadsis">Situação da Moradia</td>
                                </tr>
                                <tr>
                                    <td>Desc.:</td>
                                    <td><input type="text" name="desc" size="18"></td>
                                    <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo $moradia; ?></td>
                                </tr>
                                <input type="hidden" name="tabela" value="`moradia_situacao` (`sit_codigo`,`sit_desc`) VALUES (NULL,'">
                            </form>
                        </table>
                    </div>
                </div>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
