<?php
    include 'regras/config.php';
    include 'regras/conexao.php';

    $sql = new conexao();

    $consulta = "SELECT `nome` FROM `cadastro` WHERE `".$_POST["campo"]."` = ".$_POST[$_POST["campo"]]." ORDER BY `codigo` ASC";
    $sql->sql_consulta($consulta);

    $tabela = '<table align="center">';
    $i=1;
    while($resultado = $sql->resultado()){
        if($i==2){
            $tabela .= '<td>'.$resultado["nome"].'</td></tr>';
            $i=1;
        }else{
            $tabela .= '<tr><td>'.$resultado["nome"].'</td>';
            $i++;
        }
    }
    $tabela .='</table>';

    $consulta = "SELECT `".$_POST["campo"]."_desc` FROM ".$_POST["tabela"]." WHERE `".$_POST["campo"]."_codigo` = ".$_POST[$_POST["campo"]];
    $sql->sql_consulta($consulta);
    $nome = $sql->resultado();

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <h1>Relação de Registros</h1>
                <p style="margin: 0px; padding: 0px; text-align: center; font-weight: bold;"><?php echo $nome[$_POST["campo"]."_desc"]; ?></p>
                <?php echo $tabela; ?>
                <p style="margin: 0px; padding: 0px; text-align: center; margin-top: 10px; margin-bottom: 10px;"><a href="regras/PDFfiltro.php?codigo=<?php echo $_POST[$_POST["campo"]]."&campo=".$_POST["campo"]."&tabela=".$_POST["tabela"]; ?>">Exportar para PDF</a></p>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
