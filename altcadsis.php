<?php

include 'regras/config.php';
include 'regras/conexao.php';

$tabela[0] = "`agua`";
$campo[0] = "`agua_codigo`";
$campo2[0] = "agua_desc";
$texto[0] = "Água";

$tabela[1] = "`estado_civil`";
$campo[1] = "`ec_codigo`";
$campo2[1] = "ec_desc";
$texto[1] = "Estado Civil";

$tabela[2] = "`luz`";
$campo[2] = "`luz_codigo`";
$campo2[2] = "luz_desc";
$texto[2] = "Luz";

$tabela[3] = "`moradia_situacao`";
$campo[3] = "`sit_codigo`";
$campo2[3] = "sit_desc";
$texto[3] = "Situação da Moradia";

$tabela[4] = "`saneamento`";
$campo[4] = "`esgoto_codigo`";
$campo2[4] = "esgoto_desc";
$texto[4] = "Saneamento";

$tabela[5] = "`programa`";
$campo[5] = "`programa_codigo`";
$campo2[5] = "programa_desc";
$texto[5] = "Programas";

$tabela[6] = "`bairros`";
$campo[6] = "`bairro_codigo`";
$campo2[6] = "bairro_desc";
$texto[6] = "Bairros";

$consulta = "SELECT * FROM ".$tabela[$_GET["sis"]]." WHERE ".$campo[$_GET["sis"]]." = ".$_GET["codigo"];
$sql = new conexao();

$sql->sql_consulta($consulta);
$resultado = $sql->resultado();

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
        <script>
            function valida(form){
                if(form.desc.value==''){
                    alert("Você deve digitar a DESCRIÇÃO!");
                    form.desc.focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master" style="padding-top: 30px;">
                <table align="center" style="margin-bottom: 30px;">
                    <form action="regras/alterarsis.php" method="POST" onsubmit="return valida(this)">
                        <tr>
                            <td colspan="3" id="cadsis"><?php echo $texto[$_GET["sis"]] ?></td>
                        </tr>
                        <tr>
                            <td>Desc.:</td>
                            <td><input type="text" name="desc" size="18" value="<?php echo $resultado[$campo2[$_GET["sis"]]] ?>"> </td>
                            <td id="cadsis"><input type="submit" value="Cadastrar"></td>
                        </tr>
                        <input type="hidden" name="tabela" value="<?php echo $_GET["sis"] ?>">
                        <input type="hidden" name="codigo" value="<?php echo $_GET["codigo"] ?>">
                    </form>
                </table>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>

