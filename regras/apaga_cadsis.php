<?php

include 'config.php';
include 'conexao.php';

$tabela[0] = "`agua`";
$campo[0] = "`agua_codigo`";
$coluna[0] = "`agua` = NULL, `ea_agua` = NULL";
$condicao[0] = "`agua` = ".$_GET["codigo"]." OR `ea_agua` = ".$_GET["codigo"];

$tabela[1] = "`estado_civil`";
$campo[1] = "`ec_codigo`";
$coluna[1] = "`estado_civil` = NULL";
$condicao[1] = "`estado_civil` = ".$_GET["codigo"];

$tabela[2] = "`luz`";
$campo[2] = "`luz_codigo`";
$coluna[2] = "`luz` = NULL, `ea_luz` = NULL";
$condicao[2] = "`luz` = ".$_GET["codigo"]." OR `ea_luz` = ".$_GET["codigo"];

$tabela[3] = "`moradia_situacao`";
$campo[3] = "`sit_codigo`";
$coluna[3] = "`moradia_situacao` = NULL, `ea_situacao` = NULL";
$condicao[3] = "`moradia_situacao` = ".$_GET["codigo"]." OR `ea_situacao` = ".$_GET["codigo"];

$tabela[4] = "`saneamento`";
$campo[4] = "`esgoto_codigo`";
$coluna[4] = "`saneamento` = NULL, `ea_saneamento` = NULL";
$condicao[4] = "`saneamento` = ".$_GET["codigo"]." OR `ea_saneamento` = ".$_GET["codigo"];

$tabela[5] = "`programa`";
$campo[5] = "`programa_codigo`";
$coluna[5] = "`programa` = NULL";
$condicao[5] = "`programa` = ".$_GET["codigo"];

$tabela[6] = "`bairros`";
$campo[6] = "`bairro_codigo`";
$coluna[6] = "`bairro` = NULL, `terreno_bairro` = NULL";
$condicao[6] = "`bairro` = ".$_GET["codigo"]." OR `terreno_bairro` = ".$_GET["codigo"];

$sql = new conexao();

$consulta = "DELETE FROM ".$tabela[$_GET["sis"]]." WHERE ".$tabela[$_GET["sis"]].".".$campo[$_GET["sis"]]." = ".$_GET["codigo"];
$sql->sql_consulta($consulta);
$consulta = "UPDATE `cadastro` SET ".$coluna[$_GET["sis"]]." WHERE ".$condicao[$_GET["sis"]];
$sql->sql_consulta($consulta);

echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
echo "<script>alert('Cadastro excluído com sucesso!')</script>";
echo "<meta http-equiv='refresh' content='1; url=../cadsis.php'>";

?>
