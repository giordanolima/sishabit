<?php

include 'conexao.php';
include 'classListarPrograma.php';
include 'classListarBairros.php';

$b = $bairros->listar_array();
$p = $programas->listar_array();

$html ='
<html>
<head>
    <style>
    *{
        margin:0px;
        padding:0px;
    }
    .tudo{
        padding:0px;
        margin: 1cm;
        margin-top: 20px;
        page-break-after: always;
    }
    .ficha{
        margin:0px;
        padding: 0px;
        text-align: center;
        color: blue;
        font-size: 20pt;
    }
    .cab{
        margin:0px;
        padding:0px;
        text-align: left;
        color: black;
        font-size: 8pt;
        padding-top: 10px;
        padding-left: 10px;
        padding-right: 10px;
        margin-bottom: 20px;
    }
    table{
        border: solid 1px black;
        margin: 10px;
        width: 100%;
    }
    tr{
        border:none;
    }
    td{
        border:none;
        padding-left: 10px;
        font-size:10pt;
        text-align: left;
    }
    .titulo{
        text-align: center;
        font-weight: bold;
    }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <table class="cab" style="border: none;"><tr><td>SisHabit - Sistema de Controle Habitacional - Prefeitura Municipal de Candiota</td><td style="text-align: right">Relatório Geral</td></tr></table>
    <p class="ficha">RELATÓRIO GERAL</p>

';

$sql = new conexao();

$consulta = "SELECT `codigo`,`nome`,`endereco`,`bairro`,`programa`,`telefone`,`celular` FROM `cadastro` ORDER BY `nome`";
$sql->sql_consulta($consulta);
$i=0;
$cab = TRUE;
while($resultado = $sql->resultado()){
    if($i==6){
        $i=0;
        $html .= '</div>';
    }
    if($i==0){
        if($cab){
            $html .= '<div class="tudo">';
            $cab = FALSE;
        }else{
            $html .= '<table class="cab" style="border: none;"><tr><td>SisHabit - Sistema de Controle Habitacional - Prefeitura Municipal de Candiota</td><td style="text-align: right">Relatório Geral</td></tr></table>
                      <div class="tudo">';
        }
    }else
    $html.='
        <table>
            <tr><td>Nome:</td><td colspan=3>'.$resultado["nome"].'</td></tr>
            <tr><td>Endereço:</td><td colspan=3>'.$resultado["endereco"].'</td></tr>
            <tr><td>Bairro:</td><td>'.$b[$resultado["bairro"]].'</td><td>Programa:</td><td>'.$p[$resultado["programa"]].'</td></tr>
            <tr><td>'.$resultado["telefone"].'</td><td>'.$resultado["celular"].'</td></tr>
        </table>
    ';
    $i++;
}


$html .= '</div></body>
</html>';

//echo $html;

require_once '../dompdf/dompdf_config.inc.php';

$pdf = new DOMPDF();
$pdf->load_html($html);
$pdf->set_paper("a4");
$pdf->render();
$pdf->stream("SISHABIT - RELATÓRIO GERAL");

?>
