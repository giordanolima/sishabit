<?php


class classListarPrograma {
    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `programa`");
    }

    public function listar_combo(){
        classListarPrograma::buscar();
        $retorno = '<select name="programa" id="prog">';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["programa_codigo"].'>'.$resultado["programa_desc"].'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }

    public function listar_array(){
        classListarPrograma::buscar();
        $retorno[""] = "";
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno[$resultado["programa_codigo"]] = $resultado["programa_desc"];
        }
        return $retorno;
    }
}

$programas = new classListarPrograma();
?>
