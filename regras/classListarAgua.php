<?php

class classListarAgua {

    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `agua`");
    }

    public function listar_combo($nome){
        classListarAgua::buscar();
        if($nome==0){
            $sit = 'name="ea_agua" id="ea_h2o"';
        }else{
            $sit='name="agua" id="h2o"';
        }
        $retorno = '<select '.$sit.'>';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["agua_codigo"].'>'.$resultado["agua_desc"].'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }
}

$agua = new classListarAgua();

?>
