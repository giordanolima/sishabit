function aba(aba,lista){
            //Zerando os displays das DIVs
            document.getElementById('pessoais').style.display="none";
            document.getElementById('endereco_atual').style.display="none";
            document.getElementById('complementares').style.display="none";
            document.getElementById('conjuge').style.display="none";
            document.getElementById('filhos').style.display="none";
            document.getElementById('moradia').style.display="none";
            document.getElementById('arquivo').style.display="none";

            //Zerando os backgrounds das abas
            document.getElementById('pess').style.background="white";
            document.getElementById('end').style.background="white";
            document.getElementById('comp').style.background="white";
            document.getElementById('conj').style.background="white";
            document.getElementById('fil').style.background="white";
            document.getElementById('mor').style.background="white";
            document.getElementById('arq').style.background="white";

            //trocando
            document.getElementById(lista).style.background="lightgray";
            document.getElementById(aba).style.display="block";
}

function on_off(id){
    if (document.getElementById(id).disabled == true)
        document.getElementById(id).disabled = false;
    else{
        document.getElementById(id).value = "";
        document.getElementById(id).disabled = true;
    }
}

function valida(d){

    if(d.nome.value==''){
        alert("Você deve digitar um nome!");
        aba('pessoais','pess');
        d.nome.focus();
        return false;
    }

    return true;
}