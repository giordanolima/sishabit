<?php

class classListarSaneamento {

    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `saneamento`");
    }

    public function listar_combo($nome){
        classListarSaneamento::buscar();
        if($nome==0){
            $sit = 'name="ea_saneamento" id="ea_esgoto"';
        }else{
            $sit='name="saneamento" id="esgoto"';
        }
        $retorno = '<select '.$sit.'>';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["esgoto_codigo"].'>'.utf8_encode($resultado["esgoto_desc"]).'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }

}

$esgoto = new classListarSaneamento();

?>
