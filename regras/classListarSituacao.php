<?php

class classListarSituacao {

    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `moradia_situacao`");
    }

    public function listar_combo($nome){
        classListarSituacao::buscar();
        if($nome==0){
            $sit = 'name="ea_situacao" id="ea_sit"';
        }else{
            $sit='name="moradia_situacao" id="ms"';
        }

        $retorno = '<select '.$sit.'>';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["sit_codigo"].'>'.$resultado["sit_desc"].'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }

}

$moradia_situacao = new classListarSituacao();

?>
