<?php
    include 'config.php';
    include 'conexao.php';

    $sql = new conexao();

    $consulta = "SELECT `nome` FROM `cadastro` WHERE `".$_GET["campo"]."` = ".$_GET["codigo"]." ORDER BY `codigo` ASC";
    $sql->sql_consulta($consulta);

    $tabela = '<table align="center">';
    $i=1;
    while($resultado = $sql->resultado()){
        if($i==2){
            $tabela .= '<td>'.$resultado["nome"].'</td></tr>';
            $i=1;
        }else{
            $tabela .= '<tr><td>'.$resultado["nome"].'</td>';
            $i++;
        }
    }
    $tabela .='</table>';

    $consulta = "SELECT `".$_GET["campo"]."_desc` FROM ".$_GET["tabela"]." WHERE `".$_GET["campo"]."_codigo` = ".$_GET["codigo"];
    $sql->sql_consulta($consulta);
    $nome = $sql->resultado();

    $html = '
<html>
<head>
    <style>
    *{
        margin:0px;
        padding:0px;
    }
    .tudo{
        padding:0px;
        margin: 1cm;
        margin-top: 20px;
    }
    .ficha{
        margin:0px;
        padding: 0px;
        text-align: center;
        color: blue;
        font-size: 20pt;
    }
    .cab{
        margin:0px;
        padding:0px;
        text-align: left;
        color: black;
        font-size: 8pt;
        padding-top: 10px;
        padding-left: 10px;
    }
    table{
        border: solid 1px black;
        margin: 10px;
        width: 100%;
    }
    tr{
        border:none;
    }
    td{
        border:none;
        padding-left: 10px;
        font-size:10pt;
        text-align: left;
    }
    .titulo{
        text-align: center;
        font-weight: bold;
    }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <p class="cab">SisHabit - Sistema de Controle Habitacional - Prefeitura Municipal de Candiota</p>
    <div class="tudo">
        <p class="ficha">FILTRO POR '.caixaalta($_GET["campo"]).'</p>
        <p style="text-align: center;">'.$nome[$_GET["campo"]."_desc"].'</p>
        '.$tabela.'
     </div>
</body>
</html>
';

require_once '../dompdf/dompdf_config.inc.php';

$pdf = new DOMPDF();
$pdf->load_html($html);
$pdf->set_paper("a4");
$pdf->render();
$pdf->stream('FILTRO POR '.caixaalta($_GET["campo"]).' - '.$nome[$_GET["campo"]."_desc"]);



?>
