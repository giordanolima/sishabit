<?php

class conexao{

    var $conexao;
    var $cmd;

    private function set_conexao($conn){
        $this->conexao = $conn;
    }

    private function get_conexao(){
        return $this->conexao;
    }

    private function set_cmd($cmd){
        $this->cmd = $cmd;
    }

    private function get_cmd(){
        return $this->cmd;
    }

    public function sql_consulta_arquivo($caminho){
        $consulta='';
        $arquivo = file($caminho);
        for($i = 0;$i< count($arquivo);$i++)
            $consulta.=$arquivo[$i];
        $this->sql_consulta($consulta);
    }

    public function __construct(){
        $conn = mysqli_connect("localhost", "root", "");

        if(!$conn){
            echo "Erro ao conectar";
            exit();
        }

         if(!@mysqli_select_db($conn, "sishabit")){
            echo "Erro ao conectar no banco de dados";
            exit();
        }
		$conn->set_charset('utf8mb4');
        $this->set_conexao($conn);
    }

    public function sql_consulta ($consulta){
        $cmd = mysqli_query($this->get_conexao(), $consulta) or die ($erro = mysqli_error($this->get_conexao()));
        $this->set_cmd($cmd);
    }

    public function resultado(){
        return mysqli_fetch_array($this->get_cmd());
    }

    public function num_linhas(){
        return mysqli_num_rows($this->get_cmd());
    }

    public function retorna_indice(){
        return mysqli_insert_id($this->get_conexao());
    }
}

?>
