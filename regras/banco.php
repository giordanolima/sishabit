<?php

include 'conexao.php';
include 'config.php';
include 'classListarTudo.php';
$descricoes = new classListarTudo();
$desc = $descricoes->retorna_array();

$html = "
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <table>
    <tr>
        <td>Código
        <td>Nome
        <td>Filiação
        <td>Data de Nascimento
        <td>Naturalidade
        <td>Nacionalidade
        <td>RG
        <td>CPF
        <td>Telefone
        <td>Celular
        <td>Veiculo
        <td>Veiculo - Descrição
        <td>Email
        <td>Endereço Atual
        <td>Tipo
        <td>Cômodos
        <td>Moradores
        <td>Água
        <td>Risco
        <td>Luz
        <td>Saneamento
        <td>Situação
        <td>Bairro
        <td>Tempo de Moradia
        <td>Escolaridade
        <td>Município de Origem
        <td>Necessidades
        <td>Necessidades - Descrição
        <td>Estado Civil
        <td>Profissão
        <td>Tempo de Atividade
        <td>Empresa
        <td>Pis/Pasep
        <td>FGTS
        <td>Renda
        <td>Renda Comprovada
        <td>Cônjuge Nome
        <td>Cônjuge Data de Nascimento
        <td>Cônjuge Naturalidade
        <td>Cônjuge Nacionalidade
        <td>Cônjuge RG
        <td>Cônjuge CPF
        <td>Cônjuge Profissão
        <td>Cônjuge Local de Trabalho
        <td>Cônjuge Escolaridade
        <td>Cônjuge Renda
        <td>Cônjuge Renda Comprovada
        <td>Cônjuge Relação
        <td>Tempo de Relação
        <td>Moradia Futura - Endereço
        <td>Moradia Futura - Bairro
        <td>Moradia Futura - Tipo
        <td>Moradia Futura - Cômodos
        <td>Moradia Futura - Moradores
        <td>Moradia Futura - Água
        <td>Moradia Futura - Luz
        <td>Moradia Futura - Saneamento
        <td>Moradia Futura - Risco
        <td>Moradia Futura - Situação
        <td>Moradia Futura - Obs.
        <td>Programa
        <td>Situação
        <td>Documentos Faltantes
        <td>Documentos Arquivados
        <td>Arquivado
        <td>Prateleira
        <td>Pasta
        <td>Interesse
        <td>Observações
        <td>Data de Cadastro
        <td>Filhos
    </tr>
";
$sql = new conexao();
$filhos = new conexao();
$consulta = "SELECT * FROM `filhos`";
$filhos->sql_consulta($consulta);
while($f = $filhos->resultado()){
    $filho[$f["filho_pai"]][$f["filho_codigo"]] = $f["filhos_desc"];
}

$consulta = "SELECT * FROM `cadastro` ORDER BY `codigo`";
$sql->sql_consulta($consulta);
while($resultado = $sql->resultado()){
    $html.="
  <tr>
    <td>".$resultado["codigo"]."
    <td>".$resultado["nome"]."
    <td>".$resultado["filiacao"]."
    <td>".$resultado["nascimento"]."
    <td>".$resultado["naturalidade"]."
    <td>".$resultado["nacionalidade"]."
    <td>".$resultado["rg"]."
    <td>".$resultado["cpf"]."
    <td>".$resultado["telefone"]."
    <td>".$resultado["celular"]."
    <td>".$desc["veiculo"][$resultado["veiculo"]]."
    <td>".$resultado["veiculo_desc"]."
    <td>".$resultado["email"]."
    <td>".$resultado["endereco"]."
    <td>".$desc["construcao_tipo"][$resultado["ea_tipo"]]."
    <td>".$resultado["ea_comodos"]."
    <td>".$resultado["ea_moradores"]."
    <td>".$desc["agua"][$resultado["ea_agua"]]."
    <td>".$desc["risco"][$resultado["ea_risco"]]."
    <td>".$desc["luz"][$resultado["luz"]]."
    <td>".$desc["saneamento"][$resultado["saneamento"]]."
    <td>".$desc["sit"][$resultado["ea_situacao"]]."
    <td>".$desc["bairro"][$resultado["bairro"]]."
    <td>".$resultado["tempo_moradia"]."
    <td>".$desc["escolaridade"][$resultado["escolaridade"]]."
    <td>".$resultado["municipio_origem"]."
    <td>".$desc["necessidades"][$resultado["necessidades"]]."
    <td>".$resultado["necessidade_desc"]."
    <td>".$desc["ec"][$resultado["estado_civil"]]."
    <td>".$resultado["profissao"]."
    <td>".$resultado["tempo_atividade"]."
    <td>".$resultado["empresa"]."
    <td>".$resultado["pis_pasep"]."
    <td>".$desc["fgts"][$resultado["fgts"]]."
    <td>".$resultado["renda"]."
    <td>".$desc["renda"][$resultado["renda_comprovada"]]."
    <td>".$resultado["conjuge_nome"]."
    <td>".$resultado["conjuge_nascimento"]."
    <td>".$resultado["conjuge_naturalidade"]."
    <td>".$resultado["conjuge_nacionalidade"]."
    <td>".$resultado["rg"]."
    <td>".$resultado["cpf"]."
    <td>".$resultado["profissao"]."
    <td>".$resultado["conjuge_local_trabalho"]."
    <td>".$desc["escolaridade"][$resultado["conjuge_escolaridede"]]."
    <td>".$resultado["renda_conjuge"]."
    <td>".$desc["renda"][$resultado["renda_conjuge_comprovada"]]."
    <td>".$resultado["conjuge_relacao"]."
    <td>".$resultado["tempo_relacao"]."
    <td>".$resultado["terreno_endereco"]."
    <td>".$resultado["terreno_bairro"]."
    <td>".$desc["construcao_tipo"][$resultado["construcao_tipo"]]."
    <td>".$resultado["construcao_comodos"]."
    <td>".$resultado["construcao_moradores"]."
    <td>".$desc["agua"][$resultado["agua"]]."
    <td>".$desc["luz"][$resultado["luz"]]."
    <td>".$desc["saneamento"][$resultado["saneamento"]]."
    <td>".$desc["risco"][$resultado["moradia_risco"]]."
    <td>".$desc["sit"][$resultado["moradia_situacao"]]."
    <td>".$resultado["situacao_obs"]."
    <td>".$resultado["programa"]."
    <td>".$resultado["situacao"]."
    <td>".$resultado["documentos_faltantes"]."
    <td>".$resultado["documentos_arquivados"]."
    <td>".$desc["arquivado"][$resultado["arquivado"]]."
    <td>".$resultado["prateleira"]."
    <td>".$resultado["pasta"]."
    <td>".$resultado["interesse"]."
    <td>".$resultado["observacoes"]."
    <td>".$resultado["data_cadastro"]."
    <td>
";
    @$c = count($filho[$resultado["codigo"]]);
    @$chaves = array_keys($filho[$resultado["codigo"]]);
    for($i=0;$i<$c;$i++){
        @$html .= $filho[$resultado["codigo"]][$chaves[$i]]." / ";
    }
    $html .= "</tr>";
}

toXLS($html, "SISHABIT - Relatório Geral");

?>
