<?php
include_once 'minhasfuncoes.php';

session_start([
    "name" => "login"
]);
if (@$_SESSION["login"] != 1)
    return header("Location: " . geturl("index.php"));

$lista = '';
if($_SESSION["admin"]){
    $lista .= '<li><a href="cad.php">Novo Cadastro</a></li>
                <li><a href="cadsis.php">Cadastros Gerais</a></li>';
}

$lista .= '<li><a href="consultas.php">Consultas</a></li>
            <li><a href="filtros.php">Filtros</a></li>
            <li><a href="relatorio.php">Relatório Geral</a></li>
            <li><a href="altsenha.php">Alterar Senha</a></li>
            <li><a href="regras/sair.php">Sair</a></li>';

$menu = '
    <div class="tudo" style="padding-bottom: 0px;">
       <div class="centro" id="img">
            <h6><a href="inicial.php">SisHabit</a></h6>
            <h5>Sistema de Controle Habitacional</h5>
            <h5>Prefeitura Municipal de Candiota</h5>
       </div>
       <div class="centro" id="submenu">
            <table width="100%" style="border:none;">
            <tr><td align="left" style="border:none;">Candiota, '.DataPorExtenso().'</td><td align="right" style="border:none;">Usuário logado: <b>'.$_SESSION["user"].'</b></td></tr>
            </table>
       </div>
   </div>
   <div class="tudo" style="background: blue; padding-bottom: 0px;">
       <div class="centro" id="barra_menu">
            <ul>
                '.$lista.'
            </ul>
        </div>
    </div>
        ';

$escolaridade["combo"] = '
    <select name="escolaridade" id="esc">
        <option value=""></option>
        <option value=0>Não-Alfabetizado</option>
        <option value=1>Fundamental Incompleto</option>
        <option value=2>Fundamental Completo</option>
        <option value=3>Médio Incompleto</option>
        <option value=4>Médio Completo</option>
        <option value=5>Superior Incompleto</option>
        <option value=6>Superior Completo</option>
        <option value=7>Pós-Graduado</option>
    </select>';

$escolaridade["combo_conjuge"] = '
    <select name="conjuge_escolaridede" id="esc_c">
        <option value=""></option>
        <option value=0>Não-Alfabetizado</option>
        <option value=1>Fundamental Incompleto</option>
        <option value=2>Fundamental Completo</option>
        <option value=3>Médio Incompleto</option>
        <option value=4>Médio Completo</option>
        <option value=5>Superior Incompleto</option>
        <option value=6>Superior Completo</option>
        <option value=7>Pós-Graduado</option>
    </select>';

$escolaridade[0] = 'Não-Alfabetizado';
$escolaridade[1] = 'Fundamental Incompleto';
$escolaridade[2] = 'Fundamental Completo';
$escolaridade[3] = 'Médio Incompleto';
$escolaridade[4] = 'Médio Completo';
$escolaridade[5] = 'Superior Incompleto';
$escolaridade[6] = 'Superior Completo';
$escolaridade[7] = 'Pós-Graduado';


$construcao_tipo["combo"]["moradia"] = '
    <select name="construcao_tipo" id="ct">
        <option value=""></option>
        <option value=0>Alvenaria</option>
        <option value=1>Madeira</option>
        <option value=3>Lona</option>
        <option value=2>Mista</option>
    </select>';
$construcao_tipo["combo"]["ea"] = '
    <select name="ea_tipo" id="ea_ct">
        <option value=""></option>
        <option value=0>Alvenaria</option>
        <option value=1>Madeira</option>
        <option value=3>Lona</option>
        <option value=2>Mista</option>
    </select>';

$construcao_tipo[0] = 'Alvenaria';
$construcao_tipo[1] = 'Madeira';
$construcao_tipo[2] = 'Mista';
$construcao_tipo[3] = 'Lona';

?>
