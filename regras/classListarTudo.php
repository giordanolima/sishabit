<?php

class classListarTudo {
    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    private function buscar($tabela){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `".$tabela."`");
    }

    public function retorna_array(){
        $retorno["veiculo"][""] = "";
        $retorno["veiculo"][0] = "NÃO";
        $retorno["veiculo"][1] = "SIM";

        $retorno["necessidades"][""] = "";
        $retorno["necessidades"][0] = "NÃO";
        $retorno["necessidades"][1] = "SIM";

        $retorno["fgts"][""] = "";
        $retorno["fgts"][0] = "NÃO";
        $retorno["fgts"][1] = "SIM";

        $retorno["risco"][""] = "";
        $retorno["risco"][0] = "NÃO";
        $retorno["risco"][1] = "SIM";

        $retorno["renda"][""] = "";
        $retorno["renda"][0] = "NÃO";
        $retorno["renda"][1] = "SIM";

        $retorno["renda_c"][""] = "";
        $retorno["renda_c"][0] = "NÃO";
        $retorno["renda_c"][1] = "SIM";

        $retorno["arquivado"][""] = "";
        $retorno["arquivado"][0] = "NÃO";
        $retorno["arquivado"][1] = "SIM";

        $retorno["escolaridade"][""] = '';
        $retorno["escolaridade"][0] = 'Não-Alfabetizado';
        $retorno["escolaridade"][1] = 'Fundamental Incompleto';
        $retorno["escolaridade"][2] = 'Fundamental Completo';
        $retorno["escolaridade"][3] = 'Médio Incompleto';
        $retorno["escolaridade"][4] = 'Médio Completo';
        $retorno["escolaridade"][5] = 'Superior Incompleto';
        $retorno["escolaridade"][6] = 'Superior Completo';
        $retorno["escolaridade"][7] = 'Pós-Graduado';

        $retorno["construcao_tipo"][""] = '';
        $retorno["construcao_tipo"][0] = 'Alvenaria';
        $retorno["construcao_tipo"][1] = 'Madeira';
        $retorno["construcao_tipo"][2] = 'Mista';
        $retorno["construcao_tipo"][3] = 'Lona';

        $retorno["agua"][""] = "";
        classListarTudo::buscar("agua");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["agua"][$resultado["agua_codigo"]] = $resultado["agua_desc"];
        }

        $retorno["bairro"][""] = "";
        classListarTudo::buscar("bairros");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["bairro"][$resultado["bairro_codigo"]] = $resultado["bairro_desc"];
        }

        $retorno["ec"][""] = "";
        classListarTudo::buscar("estado_civil");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["ec"][$resultado["ec_codigo"]] = $resultado["ec_desc"];
        }

        $retorno["luz"][""] = "";
        classListarTudo::buscar("luz");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["luz"][$resultado["luz_codigo"]] = $resultado["luz_desc"];
        }

        $retorno["sit"][""] = "";
        classListarTudo::buscar("moradia_situacao");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["sit"][$resultado["sit_codigo"]] = $resultado["sit_desc"];
        }

        $retorno["programa"][""] = "";
        classListarTudo::buscar("programa");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["programa"][$resultado["programa_codigo"]] = $resultado["programa_desc"];
        }

        $retorno["saneamento"][""] = "";
        classListarTudo::buscar("saneamento");
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno["saneamento"][$resultado["esgoto_codigo"]] = $resultado["esgoto_desc"];
        }

        return $retorno;

    }
}
?>
