<?php

    function geturl($path)
    {
        return "http://localhost/sishabit/$path";
    }

    /**Função que força o download de um documento HTML no formato DOC
     * @param string $documento Documento a ser salvo
     * @param string $nome Nome do documento a ser salvo
     **/
    function toDOC($documento,$nome){
        header("Content-type: application/vnd.ms-word");
        header("Content-type: application/force-download");
        header("Content-Disposition: attachment; filename=".$nome.".doc");
        header("Pragma: no-cache");
        echo $documento;
        exit();
    }

    /**Função que força o download de uma tabela HTML no formato XLS
     * @param string $tabela Planilha a ser salvo
     * @param string $nome Nome do documento a ser salvo
     **/
    function toXLS($tabela,$nome){
        header("Content-type: application/vnd.ms-excel");
        header("Content-type: application/force-download");
        header("Content-Disposition: attachment; filename=".$nome.".xls");
        header("Pragma: no-cache");
        echo $tabela;
        exit();
	}

    /** Função para transformar uma data criada aparitr do mktime() no formato xx Anos e yy Meses
     * @param int $tempototal Tempo a ser tranformado
     * @return string Data descrita
     **/
    function DescreveData($tempototal) {
        $anos = intval(($tempototal/60/60/24/365));
        $meses = intval(((($tempototal/60/60/24/365) - $anos)*365)/30);

        if ($anos !=0)
            {
            if ($anos ==1)
                $ano = '1 ANO E';
                else
                $ano = $anos.' ANOS E';
            }else
                $ano = '';

        if ($meses !=0)
            {
            if ($meses ==1)
                $tt = $ano.' 1 M�S';
                else
                $tt = $ano.' '.$meses.' MESES';
            }else
            $tt = $ano.'';



        return $tt;
    }

    /**Transforma datas Americanas em Brasilerias e vice-versa
     * @param string $data Data a ser transformada
     * @param string $delimitador Caracter que separa data null="/"
     * @return string Nova data formatada
     **/
    function TransformaData($data,$delimitador = "/"){
        $partes = explode($delimitador, $data);
        return $partes[2].$delimitador.$partes[1].$delimitador.$partes[0];
    }

    /**Função para enviar emails sem anexo
     * @param string $destinatario Email do destinatário
     * @param string $remetente Email do remetente. Deve ser do mesmo domínio do site.
     * @param string $assunto Assunto do email
     * @param string $men Corpo da Mensagem
     **/
    function EnviarEmail($destinatario,$remetente,$assunto,$men){
        $headers = 'MIME-Version: 1.0 \r\n';
        $headers .= 'Content-type: text/html; charset=iso-8859-1 \r\n';
        $headers .= 'From: '.$remetente.' \r\n';
        mail($destinatario,$assunto,$men, $headers);
    }

    function DataPorExtenso ($timestamp = null){
        if ($timestamp==NULL)
            $timestamp = time();

        $mes["01"]='Janeiro';
        $mes["02"]='Fevereiro';
        $mes["03"]='Março';
        $mes["04"]='Abril';
        $mes["05"]='Maio';
        $mes["06"]='Junho';
        $mes["07"]='Julho';
        $mes["08"]='Agosto';
        $mes["09"]='Setembro';
        $mes["10"]='Outubro';
        $mes["11"]='Novembro';
        $mes["12"]='Dezembro';

        return strftime("%d",$timestamp).' de '.$mes[strftime("%m",$timestamp)].' de '.strftime("%Y",$timestamp);
    }

    function caixaalta($string){

        $retorno = strtoupper($string);
        $localizar[1] = 'ã';
        $localizar[2] = 'õ';
        $localizar[3] = 'á';
        $localizar[4] = 'é';
        $localizar[5] = 'í';
        $localizar[6] = 'ó';
        $localizar[7] = 'ú';
        $localizar[8] = 'à';
        $localizar[9] = 'â';
        $localizar[10] = 'ê';
        $localizar[11] = 'î';
        $localizar[12] = 'ô';
        $localizar[13] = 'û';
        $localizar[14] = 'ü';
        $localizar[15] = 'ç';
        $substituir[1] = 'Ã';
        $substituir[2] = 'Õ';
        $substituir[3] = 'Á';
        $substituir[4] = 'É';
        $substituir[5] = 'Í';
        $substituir[6] = 'Ó';
        $substituir[7] = 'Ú';
        $substituir[8] = 'À';
        $substituir[9] = 'Â';
        $substituir[10] = 'Ê';
        $substituir[11] = 'Î';
        $substituir[12] = 'Ô';
        $substituir[13] = 'Û';
        $substituir[14] = 'Ü';
        $substituir[15] = 'Ç';

        $retorno = str_replace($localizar, $substituir, $retorno);
        return $retorno;
    }

?>
