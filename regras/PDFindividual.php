<?php

include 'conexao.php';
include 'classListarTudo.php';

$sql = new conexao();
$descricoes = new classListarTudo();
$desc = $descricoes->retorna_array();

$consulta = "SELECT * FROM `cadastro` WHERE `codigo` = ".$_GET["codigo"];
$sql->sql_consulta($consulta);
$resultado = $sql->resultado();

$consulta = "SELECT * FROM `filhos` WHERE `filho_pai` = ".$_GET["codigo"];
$sql->sql_consulta($consulta);
$filhos = '';
if($sql->num_linhas()==0)
    $filhos .= "<tr><td align='center' colspan=2>Não há filhos.</td></tr>";

$i=1;
while($result = $sql->resultado()){
    if($i==2){
        $i=1;
        $filhos .= "<td>".$result["filhos_desc"]."</td></tr>";
    }else{
        $filhos .= "<tr><td>".$result["filhos_desc"]."</td>";
        $i++;
    }
}

//Transformando as rendas para duas casas decimais
    if($resultado["renda"]!='""'){
        $resultado["renda"] = number_format($resultado["renda"],2);
        $partes = explode(",", $resultado["renda"]);
        $resultado["renda"] = '';
        for($i=0;$i<count($partes);$i++)
            $resultado["renda"] .= $partes[$i];
    }
    else
        $resultado["renda"] = '';

    if($resultado["renda_conjuge"]!='""'){
        $resultado["renda_conjuge"] = number_format($resultado["renda_conjuge"],2);
        $partes = explode(",", $resultado["renda_conjuge"]);
        $resultado["renda_conjuge"] = '';
        for($i=0;$i<count($partes);$i++)
            $resultado["renda_conjuge"] .= $partes[$i];
    }
    else
        $resultado["renda_conjuge"] = '';

$html = '
<html>
<head>
    <style>
    *{
        margin:0px;
        padding:0px;
    }
    .tudo{
        padding:0px;
        margin: 1cm;
        margin-top: 20px;
        page-break-after: always;
    }
    .ficha{
        margin:0px;
        padding: 0px;
        text-align: center;
        color: blue;
        font-size: 20pt;
    }
    .cab{
        margin:0px;
        padding:0px;
        text-align: left;
        color: black;
        font-size: 8pt;
        padding-top: 10px;
        padding-left: 10px;
        padding-right: 10px;
    }
    table{
        border: solid 1px black;
        margin: 10px;
        width: 100%;
    }
    tr{
        border:none;
    }
    td{
        border:none;
        padding-left: 10px;
        font-size:10pt;
        text-align: left;
    }
    .titulo{
        text-align: center;
        font-weight: bold;
    }
    </style>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <table class="cab" style="border: none;"><tr><td>SisHabit - Sistema de Controle Habitacional - Prefeitura Municipal de Candiota</td><td style="text-align: right">'.$resultado["nome"].'</td></tr></table>
    <div class="tudo">
      <p class="ficha">FICHA INDIVIDUAL</p>
        <p style="text-align: right;">Data de Cadastro: '.$resultado["data_cadastro"].'</p>
        <table align="center">
            <tr><td colspan="4" align="center">DADOS PESSOAIS</td> </tr>
            <tr><td>Nome:</td><td colspan="3">'.$resultado["nome"].'</td> </tr>
            <tr><td>Filiação:</td><td colspan="3">'.$resultado["filiacao"].'</td> </tr>
            <tr><td>Nascimento:</td><td>'.$resultado["nascimento"].'</td> <td>Estado Civil:</td> <td>'.$desc["ec"][$resultado["estado_civil"]].'</td> </tr>
            <tr><td>Naturalidade:</td><td>'.$resultado["naturalidade"].'</td><td>Nacionalidade:</td><td>'.$resultado["nacionalidade"].'</td> </tr>
            <tr><td>RG:</td><td>'.$resultado["rg"].'</td><td>CPF:</td><td>'.$resultado["cpf"].'</td> </tr>
            <tr><td>Profissão:</td><td>'.$resultado["profissao"].'</td><td>Tempo de atividade:</td><td>'.$resultado["tempo_atividade"].'</td> </tr>
            <tr><td>Empresa:</td><td colspan="3">'.$resultado["empresa"].'</td> </tr>
            <tr><td>FGTS:</td><td>'.$desc["fgts"][$resultado["fgts"]].'</td><td>PIS/PASEP:</td><td>'.$resultado["pis_pasep"].'</td> </tr>
        </table>
        <table align="center">
            <tr><td colspan="4" align="center">ENDEREÇO ATUAL</td></tr>
            <tr><td>Endereço:</td><td>'.$resultado["endereco"].'</td><td>Bairro:</td><td>'.$desc["bairros"][$resultado["bairro"]].'</td></tr>
            <tr><td>Cidade:</td><td>'.$resultado["cidade"].'</td><td>Município de Origem:</td><td>'.$resultado["municipio_origem"].'</td></tr>
            <tr><td>Tempo de Moradia:</td><td>'.$resultado["tempo_moradia"].'</td><td>Situação:</td><td>'.$desc["sit"][$resultado["ea_situacao"]].'</td></tr>
            <tr><td>Tipo de Construção:</td><td>'.$desc["construcao_tipo"][$resultado["ea_tipo"]].'</td><td>Luz:</td><td>'.$desc["luz"][$resultado["ea_luz"]].'</td></tr>
            <tr><td>Número de Cômodos:</td><td>'.$resultado["ea_comodos"].'</td><td>Número de Moradores:</td><td>'.$resultado["ea_moradores"].'</td> </tr>
            <tr><td>Água:</td><td>'.$desc["agua"][$resultado["ea_agua"]].'</td><td>Risco:</td><td>'.$desc["risco"][$resultado["ea_risco"]].'</td></tr>
            <tr><td>Saneamento:</td><td>'.$desc["saneamento"][$resultado["saneamento"]].'</td><td>Possui Terreno:</td><td>'.$desc["veiculo"][$resultado["terreno"]].' - '.$resultado["terreno_desc"].'</td></tr>
                <tr><td>Local do Terreno</td><td>'.$resultado["local_terreno"].'</td><td>Tempo de Aluguel:</td><td>'.$resultado["tempo_aluguel"].'</td></tr>
        </table>
        <table>
            <tr><td colspan="4" class="titulo">DADOS COMPLEMENTARES</td> </tr>
            <tr><td><b>Telefone:</b></td><td>'.$resultado["telefone"].'</td><td><b>Celular:</b></td><td>'.$resultado["celular"].'</td> </tr>
            <tr><td><b>Email:</b></td><td>'.$resultado["telefone"].'</td><td><b>Veículo:</td><td>'.$desc["veiculo"][$resultado["veiculo"]].' - '.$resultado["veiculo_desc"].'</td> </tr>
            <tr><td><b>Escolaridade:</td><td>'.$desc["escolaridade"][$resultado["escolaridade"]].'</td><td><b>Necessidades Especiais:</td><td>'.$desc["necessidades"][$resultado["necessidades"]].' - '.$resultado["necessidade_desc"].'</td> </tr>
            <tr><td><b>Renda:</td><td>'.$resultado["renda"].'</td><td><b>Renda Comprovada:</td><td>'.$desc["renda"][$resultado["renda_comprovada"]].'</td></tr>
        </table>
        <table>
            <tr><td colspan="4" class="titulo">DADOS DO(A) CONJUGE</td> </tr>
            <tr><td><b>Nome:</td><td colspan="3">'.$resultado["conjuge_nome"].'</td> </tr>
            <tr><td><b>Nascimento:</td><td>'.$resultado["conjuge_nascimento"].'</td><td><b>Profissão:</td><td>'.$resultado["conjuge_profissao"].'</td></tr>
            <tr><td><b>Local de Trabalho:</td><td>'.$resultado["conjuge_local_trabalho"].'</td><td><b>Escolaridade:</td><td>'.$desc["escolaridade"][$resultado["conjuge_escolaridede"]].'</td></tr>
            <tr><td><b>Naturalidade:</td><td>'.$resultado["conjuge_naturalidade"].'</td><td><b>Nacionalidade:</td><td>'.$resultado["conjuge_nacionalidade"].'</td> </tr>
            <tr><td><b>RG:</td><td>'.$resultado["conjuge_rg"].'</td><td><b>CPF:</td><td>'.$resultado["conjuge_cpf"].'</td> </tr>
            <tr><td><b>Tipo de Relação:</td><td>'.$resultado["conjuge_relacao"].'</td><td><b>Tempo de Relação:</td><td>'.$resultado["tempo_relacao"].'</td> </tr>
            <tr><td><b>Renda:</td><td>'.$resultado["renda_conjuge"].'</td><td><b>Renda Comprovada:</td><td>'.$desc["renda"][$resultado["renda_conjuge_comprovada"]].'</td></tr>
        </table>
        <table>
            <tr><td colspan=2 class="titulo">FILHOS</td></tr>
            '.$filhos.'
        </table>
    </div>
    <table class="cab" style="border: none; margin-bottom: 20px;"><tr><td>SisHabit - Sistema de Controle Habitacional - Prefeitura Municipal de Candiota</td><td style="text-align: right">'.$resultado["nome"].'</td></tr></table>
    <div class="tudo">
        <table>
            <tr><td colspan=4 class="titulo">DADOS DA MORADIA</td> </tr>
            <tr><td><b>Endereço:</td><td colspan="3">'.$resultado["terreno_endereco"].'</td> </tr>
            <tr><td><b>Bairro:</td><td>'.$desc["bairro"][$resultado["terreno_bairro"]].'</td><td><b>Tipo de Construção:</td><td>'.$desc["construcao_tipo"][$resultado["construcao_tipo"]].'</td></tr>
            <tr><td><b>Número de Cômodos:</td><td>'.$resultado["construcao_comodos"].'</td><td><b>Número de Moradores:</td><td>'.$resultado["construcao_moradores"].'</td> </tr>
            <tr><td><b>Luz:</td><td>'.$desc["luz"][$resultado["luz"]].'</td><td><b>Água:</td><td>'.$desc["agua"][$resultado["agua"]].'</td> </tr>
            <tr><td><b>Risco:</td><td>'.$desc["risco"][$resultado["moradia_risco"]].'</td><td><b>Saneamento:</td><td>'.$desc["saneamento"][$resultado["saneamento"]].'</td></tr>
            <tr><td><b>Situação:</td><td colspan="3">'.$desc["sit"][$resultado["moradia_situacao"]].'</td></tr>
            <tr><td><b>Observação:</td><td colspan="3">'.$resultado["situacao_obs"].'</td> </tr>
        </table>
        <table>
            <tr><td colspan="6" class="titulo">DADOS DE ARQUIVO</td> </tr>
            <tr><td><b>Programa:</td><td colspan="5">'.$desc["programa"][$resultado["programa"]].'</td> </tr>
            <tr><td><b>Situação:</td><td colspan="3">'.$resultado["situacao"].'</td><td><b>Interesse:</td><td>'.$resultado["interesse"].'</td></tr>
            <tr><td><b>Documentos Faltantes:</td><td colspan="5">'.$resultado["documentos_faltantes"].'</td> </tr>
            <tr><td><b>Documentos Arquivados:</td><td colspan="5">'.$resultado["documentos_arquivados"].'</td> </tr>
            <tr><td><b>Arquivado:</td><td>'.$desc["arquivado"][$resultado["arquivado"]].'</td><td><b>Prateleira:</td><td>'.$resultado["prateleira"].'</td><td><b>Pasta:</td><td>'.$resultado["pasta"].'</td></tr>
            <tr><td><b>Observação:</td><td colspan="5">'.$resultado["observacoes"].'</td> </tr>
        </table>
     </div>
</body>
</html>
';

//echo $html;

require_once '../dompdf/dompdf_config.inc.php';

$pdf = new DOMPDF();
$pdf->load_html($html);
$pdf->set_paper("a4");
$pdf->render();
$pdf->stream("FICHA INDIVIDUAL - ".$resultado["nome"]);


?>
