<?php


class classListarBairros {
    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `bairros`");
    }

    public function listar_combo($campo){
        switch ($campo){
            case 0: $nome = 'name="bairro" id="bai"';break;
            case 1: $nome = 'name="terreno_bairro" id="ter_bai"';break;
        }
        classListarBairros::buscar();
        $retorno = '<select '.$nome.' >';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["bairro_codigo"].'>'.$resultado["bairro_desc"].'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }

    public function listar_array(){
        classListarBairros::buscar();
        $retorno[""] = "";
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno[$resultado["bairro_codigo"]] = $resultado["bairro_desc"];
        }
        return $retorno;
    }

}

$bairros = new classListarBairros();

?>
