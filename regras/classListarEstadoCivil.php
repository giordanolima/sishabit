<?php

class classListarEstadoCivil {

    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);        
    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `estado_civil`");
    }

    public function listar_combo(){
        classListarEstadoCivil::buscar();
        $retorno = '<select name="estado_civil" id="ec">';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["ec_codigo"].'>'.$resultado["ec_desc"].'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }

}

$estado_civil = new classListarEstadoCivil();

?>
