<?php

class classListarLuz {

    var $obj_sql;

    private function set_obj_sql($obj){
        $this->obj_sql = $obj;
    }

    private function get_obj_sql(){
        return $this->obj_sql;
    }

    public function __construct(){
        $sql = new conexao();
        $this->set_obj_sql($sql);

    }

    public function buscar(){
        $this ->get_obj_sql() -> sql_consulta("SELECT * FROM `luz`");
    }

    public function listar_combo($nome){
        classListarLuz::buscar();
        if($nome==0){
            $sit = 'name="ea_luz" id="ea_luzz"';
        }else{
            $sit='name="luz" id="luzz"';
        }
        $retorno = '<select '.$sit.'>';
        $retorno.= '<option value=""></option>';
        while ($resultado = $this ->get_obj_sql() -> resultado() ){
            $retorno .= '<option value='.$resultado["luz_codigo"].'>'.$resultado["luz_desc"].'</option>';
        }
        $retorno .= '</select>';
        return $retorno;
    }

}

$luz = new classListarLuz();

?>
