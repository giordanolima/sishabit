<?php
    include 'regras/config.php';
    $cadastro='';
    if($_SESSION["admin"]){
        $cadastro ='<div class="fleft" style="border: 2px solid blue;">
                    <div>
                        <H1>NOVO CADASTRO</H1>
                    </div>
                    <div class="fleft">
                        <a href="cad.php"><img src="imagem/cad.jpg" style="width: 150px; height: 150px;"></a>
                    </div>
                    <div class="fleft" style="margin: 0px; padding: 0px;">
                        <p style="width: 120px; margin: 0px; padding: 0px;">Para novos cadastros no sistema</p>
                        <p><a href="cad.php">Clique Aqui</a></p>
                    </div>
                </div>';
    }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master" style="padding-top: 30px;">
                <?php echo $cadastro; ?>
                <div class="fleft" style="border: 2px solid blue;">
                    <div>
                        <H1>CONSULTAS</H1>
                    </div>
                    <div class="fleft">
                        <a href="consultas.php"><img src="imagem/consultas.jpg" style="width: 150px; height: 150px;"></a>
                    </div>
                    <div class="fleft" style="margin: 0px; padding: 0px;">
                        <p style="width: 120px; margin: 0px; padding: 0px;">Para consultas no Banco de Dados</p>
                        <p><a href="consultas.php">Clique Aqui</a></p>
                    </div>
                </div>
                <div class="fleft" style="border: 2px solid blue;">
                    <div>
                        <H1>Relatório Geral</H1>
                    </div>
                    <div class="fleft">
                        <a href="relatorio.php"><img src="imagem/relatorios.jpg" style="width: 150px; height: 150px;"></a>
                    </div>
                    <div class="fleft" style="margin: 0px; padding: 0px;">
                        <p style="width: 120px; margin: 0px; padding: 0px;">Relatório geral e detalhado de todos os registros do sistema.</p>
                        <p><a href="relatorio.php">Clique Aqui</a></p>
                    </div>
                </div>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
