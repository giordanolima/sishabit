<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php
    session_start([
    "name" => "login"
]);
    if ($_SESSION["login"]!=2){
        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=../index.php'>";
    }

    include '../regras/conexao.php';
    $sql = new conexao();

    $admin[0] = "Usuário";
    $admin[1] = "Administrador";

    $tabela = null;
    $sql -> sql_consulta("SELECT * FROM `login`");
    while ($result = $sql -> resultado()){
        $tabela .= "<tr><td>".$result["login"]."</td><td>".$admin[$result["admin"]]."</td><td><a href='painelsenha.php?login=".$result["login"]."'>Alterar Senha</a></td><td><a href='exclogin.php?login=".$result["login"]."' onclick='return valida();'>Excluir</a></td></tr>";
    }

?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="../regras/estilo.css">
        <script type="text/javascript">
            function valida(){
                if (confirm("Você tem certeza que deseja excluir este registro?"))
                {
		return true;
		}else{
		return false;
		}
            }
        </script>
    </head>
    <body>
        <div class="tudo" style="background: white;">
            <div class="centro" id="master" style="padding-top: 30px; background: none;">
                <H1>PAINEL DE CONTROLE DE USUARIOS</H1>
                    <center>
                    <table>
                        <?php
                        if($tabela != null)
                            echo $tabela;
                        ?>
                        <tr><td colspan="4" align="center"><a href="cadlogin.php">Cadastrar login</a></td></tr>
                        <tr><td colspan="4" align="center"><a href="../regras/sair.php">Sair</a></td></tr>
                    </table>
                    </center>
            </div>
        </div>
    </body>
</html>
