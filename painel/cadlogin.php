<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php
    session_start([
    "name" => "login"
]);
    if ($_SESSION["login"]!=2){
        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=../index.php'>";
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="../regras/estilo.css">
        <script>
            function valida(){
                if (document.cadsenha.login.value == "")
                    {
                        alert ("Preencha o login!");
                        document.cadsenha.login.focus();
                        return false;
                    }else if (document.cadsenha.senha.value == "")
                        {
                        alert ("Preencha a senha!");
                        document.cadsenha.senha.focus();
                        return false;
                        }else if(document.cadsenha.senha.value != document.cadsenha.rsenha.value)
                                {
                                    alert ("Repita sua senha corretamente!");
                                    document.cadsenha.rsenha.focus();
                                    return false;
                                }
                 return true;
            }
        </script>
    </head>
    <body>
        <div class="tudo" style="background: white;">
            <div class="centro" id="master" style="padding-top: 30px; background: none;">
            <H1>PAINEL DE CONTROLE DE USUARIOS</H1>
                <center>
                <table>
                    <form action="cadastrarlogin.php" name="cadsenha" method="post" onsubmit="return valida()">
                    <tr><td>Login:</td> <td><input type="text" name="login" size="20"></td> </tr>
                    <tr><td>Perfil:</td> <td><select name="admin"> <option value="0">Usuário</option><option value="1">Administrador</option> </select> </td> </tr>
                    <tr><td>Senha:</td><td><input type="password" name="senha" size="20"></td></tr>
                    <tr><td>Repita sua senha:</td><td><input type="password" name="rsenha" size="20"></td></tr>
                    <tr> <td colspan="2" align="center"><input type="submit" value="Cadastrar Login"></td> </tr>
                    </form>
                </table>
                    <p><a href="index.php">Voltar</a></p>
                </center>
            </div>
        </div>
    </body>
</html>
