<?php
include 'regras/minhasfuncoes.php';
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Educacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
    </head>
    <body>
        <div class="tudo" style="background: blue;margin: 0px; padding: 3px;">
            <div class="centro">
                <p style="margin: 0px; padding: 0px; text-align: right; color: white;">Candiota, <?php echo DataPorExtenso(); ?></p>
            </div>
        </div>
        <div class="tudo" style="height: 400px;">
            <div class="centro" id="login">
                <div class="fleft" style="width: 430px;">
                    <div style="margin-top: 30px; text-align: center; height: 350px;">
                        <img src="imagem/sishabit.jpg" style="width: 430px; height: 313px; margin-top: 30px;">
                    </div>
                </div>
                <div class="fleft" style="text-align: center; width: 430px;">
                    <p style="font-family: 'Comic Sans MS';font-size: 32px; color: blue;">SisHabit</p>
                    <p style="font-family: 'Times New Roman'; font-size: 20px;">Sistema de Controle Habitacional</p>
                    <p style="font-family: 'Times New Roman'; font-size: 20px;">Prefeitura Municipal de Candiota</p>
                    <div style="text-align: center;">
                        <center>
                            <form action="regras/login.php" method="POST" style="border: 2px blue solid; padding: 20px; background: rgb(232,238,250)">
                                <p style="margin: 0px; padding: 0px; margin-bottom: 10px; font-size: 13pt;">Acesse o sistema com seu login e senha:</p>
                                <table id="tabela_inicial">
                                <tr><td>Login:</td><td><input type="text" size="20" name="login"></td></tr>
                                <tr><td>Senha:</td> <td><input type="password" size="20" name="senha"></td></tr>
                                <tr><td colspan="2"><center><input type="submit" value="Login"><input type="reset" value="Reset"></center></td></tr>
                                </table>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="tudo" style="background: blue;margin: 0px; padding: 3px;">
            <div class="centro">
                <p style="margin: 0px; padding: 0px; text-align: center; color: white;">Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank" style="color: white;">PALMI Inform�tica</a>. Todos os direitos reservados.</p>
            </div>
        </div>
    </body>
</html>
