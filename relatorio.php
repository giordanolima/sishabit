<?php
    include 'regras/config.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>RELATÓRIOS GERAIS</H1>
                <div class="fleft" id="consultas" style="width: 420px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Relatório Geral:</p>
                    <p><a href="regras/relgeral.php">Exportar para PDF</a></p>
                </div>
                <div class="fleft" id="consultas" style="width: 420px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Banco de Dados:</p>
                    <p><a href="regras/banco.php">Exportar para XLS</a></p>
                </div>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
