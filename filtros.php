<?php
    include 'regras/config.php';
    include 'regras/conexao.php';
    include 'regras/classListarBairros.php';
    include 'regras/classListarPrograma.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>Filtros</H1>
                <div class="fleft" id="consultas" style="width: 420px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Selecione o Bairro:</p>
                    <form action="exibirfiltro.php" method="post" onsubmit="if(this.bairro.value=='') {alert('Selecione um bairro!'); return false;}">
                        <table align="center">
                            <tr><td><?php echo $bairros->listar_combo(0); ?></td><td><input type="submit" value="Avançar"></td></tr>
                        </table>
                        <input type="hidden" value="bairro" name="campo">
                        <input type="hidden" value="bairros" name="tabela">
                    </form>
                </div>
                <div class="fleft" id="consultas" style="width: 420px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Selecione o Programa:</p>
                    <form action="exibirfiltro.php" method="post" onsubmit="if(this.programa.value=='') {alert('Selecione um programa!'); return false;}">
                        <table align="center">
                            <tr><td><?php echo $programas->listar_combo(); ?></td><td><input type="submit" value="Avançar"></td></tr>
                        </table>
                        <input type="hidden" value="programa" name="campo">
                        <input type="hidden" value="programa" name="tabela">
                    </form>
                </div>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
