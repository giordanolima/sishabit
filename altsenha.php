<?php
include 'regras/config.php';
include 'regras/conexao.php';
$sql = new conexao();

$consulta = "SELECT * FROM `login` WHERE `login` = '".$_SESSION["user"]."'";
$sql->sql_consulta($consulta);
$resultado = $sql->resultado();

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
        <script>
            function validasenha(){

                if(document.altsenha.senhaa.value != '<?php echo $resultado["senha"]; ?>'){
                    alert ("Senha antiga incorreta!");
                    document.altsenha.senhaa.focus();
                    return false;
                }

                if(document.altsenha.senha.value == ''){
                    alert ("Você deve digitar uma senha!");
                    document.altsenha.senha.focus();
                    return false;
                }

                if(document.altsenha.senha.value != document.altsenha.rsenha.value){
                    alert ("Repita sua senha corretamente!");
                    document.altsenha.rsenha.focus();
                    return false;
                }
             return true;
            }
        </script>
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>ALTERAR SENHA</H1>
                <table align="center">
                    <form action="regras/alterar_senha.php" name="altsenha" method="post" onsubmit="return validasenha()">
                        <tr><td>Login:</td> <td><center><b><?php echo $_SESSION["user"]; ?></b></center></td> </tr>
                        <tr><td>Senha Antiga:</td><td><input type="password" name="senhaa" size="20"></td></tr>
                        <tr><td>Senha Nova:</td><td><input type="password" name="senha" size="20"></td></tr>
                        <tr><td>Repita sua senha:</td><td><input type="password" name="rsenha" size="20"></td></tr>
                        <tr> <td colspan="2" align="center"><input type="submit" value="Alterar"></td> </tr>
                    </form>
                </table>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>