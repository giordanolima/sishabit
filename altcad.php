<?php
    include 'regras/config.php';
    include 'regras/conexao.php';
    include 'regras/classListarEstadoCivil.php';
    include 'regras/classListarAgua.php';
    include 'regras/classListarLuz.php';
    include 'regras/classListarSaneamento.php';
    include 'regras/classListarSituacao.php';
    include 'regras/classListarPrograma.php';
    include 'regras/classListarBairros.php';

    $sql = new conexao();

    $consulta = "SELECT * FROM `cadastro` WHERE `codigo` = ".$_GET["codigo"];
    $sql->sql_consulta($consulta);
    $resultado = $sql->resultado();

    $chaves = array_keys($resultado);
    for($i=0;$i<count($resultado);$i++){
        if($resultado[$chaves[$i]]=='')
            $resultado[$chaves[$i]]='""';
    }

//Transformando as rendas para duas casas decimais
    if($resultado["renda"]!='""'){
        $resultado["renda"] = number_format($resultado["renda"],2);
        $partes = explode(",", $resultado["renda"]);
        $resultado["renda"] = '';
        for($i=0;$i<count($partes);$i++)
            $resultado["renda"] .= $partes[$i];
    }
    else
        $resultado["renda"] = '';

    if($resultado["renda_conjuge"]!='""'){
        $resultado["renda_conjuge"] = number_format($resultado["renda_conjuge"],2);
        $partes = explode(",", $resultado["renda_conjuge"]);
        $resultado["renda_conjuge"] = '';
        for($i=0;$i<count($partes);$i++)
            $resultado["renda_conjuge"] .= $partes[$i];
    }
    else
        $resultado["renda_conjuge"] = '';

//Retransformando os textareas
if($resultado["documentos_faltantes"]=='""')
    $resultado["documentos_faltantes"]='';
if($resultado["documentos_arquivados"]=='""')
    $resultado["documentos_arquivados"]='';

$consulta = 'SELECT `filhos_desc` FROM  `filhos` WHERE `filho_pai` = '.$_GET["codigo"];
$sql->sql_consulta($consulta);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
        <script type="text/javascript" src="regras/js/script.js"></script>
        <script type="text/javascript" src="regras/js/select_options.js"></script>
        <script type="text/javascript">
        function selecionaCombo(id,valor){
            obj = document.getElementById(id);
            for (i=0;i<obj.length;i++){
                if(obj.options[i].value==valor && obj.options[i].value!= ""){
                    obj.options[i].selected = 'TRUE';
                    return;
                }
            }
        }

        function selecionaCheck(id,valor,campo){
            obj = document.getElementById(id);
            if(valor==1){
                obj.checked = 'TRUE';
                on_off(campo);
            }
        }
        
        function selecionaTudo(){
            selecionaCheck("carro",<?php echo $resultado["veiculo"]; ?>,"veiculoo");
            selecionaCheck("nec",<?php echo $resultado["necessidades"]; ?>,"necessidadess");
            selecionaCheck("ter",<?php echo $resultado["terreno"]; ?>,"terrenoo");
            selecionaCombo("ec",<?php echo $resultado["estado_civil"]; ?>);
            selecionaCombo("esc",<?php echo $resultado["escolaridade"]; ?>);
            selecionaCombo("esc_c",<?php echo $resultado["conjuge_escolaridede"]; ?>);
            selecionaCombo("fundo",<?php echo $resultado["fgts"]; ?>);
            selecionaCombo("rc",<?php echo $resultado["renda_comprovada"]; ?>);
            selecionaCombo("rcc",<?php echo $resultado["renda_conjuge_comprovada"]; ?>);
            selecionaCombo("ct",<?php echo $resultado["construcao_tipo"]; ?>);
            selecionaCombo("ea_ct",<?php echo $resultado["ea_tipo"]; ?>);
            selecionaCombo("h2o",<?php echo $resultado["agua"]; ?>);
            selecionaCombo("ea_h2o",<?php echo $resultado["ea_agua"]; ?>);
            selecionaCombo("luzz",<?php echo $resultado["luz"]; ?>);
            selecionaCombo("ea_luzz",<?php echo $resultado["ea_luz"]; ?>);
            selecionaCombo("esgoto",<?php echo $resultado["saneamento"]; ?>);
            selecionaCombo("ea_esgoto",<?php echo $resultado["ea_saneamento"]; ?>);
            selecionaCombo("mr",<?php echo $resultado["moradia_risco"]; ?>);
            selecionaCombo("mr",<?php echo $resultado["moradia_risco"]; ?>);
            selecionaCombo("ea_risc",<?php echo $resultado["ea_risco"]; ?>);
            selecionaCombo("ms",<?php echo $resultado["moradia_situacao"]; ?>);
            selecionaCombo("ea_sit",<?php echo $resultado["ea_situacao"]; ?>);
            selecionaCombo("arquivadoo",<?php echo $resultado["arquivado"]; ?>);
            selecionaCombo("prog",<?php echo $resultado["programa"]; ?>);
            selecionaCombo("bai",<?php echo $resultado["bairro"]; ?>);
            selecionaCombo("ter_bai",<?php echo $resultado["terreno_bairro"]; ?>);
            <?php
                while($resultado_filhos = $sql->resultado()){
                        echo 'cad_filho("'.$resultado_filhos["filhos_desc"].'");';
                }
                echo'
';
            ?>
            aba('pessoais','pess');
        }

        </script>
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>ALTERAR CADASTRO</H1>
                <form name="cadastro" action="regras/alterar_registro.php" method="post" onsubmit="return valida(this);" id="cad">
                <ul class="menu-cad" align="center">
                        <li id="pess">
                            <a href="#pessoais" onclick="aba('pessoais','pess')">Dados Pessoais</a>
                        </li>
                        <li id="end">
                            <a href="#endereco_atual" onclick="aba('endereco_atual','end')">Endereço Atual</a>
                        </li>
                        <li id="comp">
                            <a href="#complementares" onclick="aba('complementares','comp')">Dados Complementares</a>
                        </li>
                        <li id="conj">
                            <a href="#conjuge" onclick="aba('conjuge','conj')">Dados do(a) Conjuge</a>
                        </li>
                        <li id="fil">
                            <a href="#filhos" onclick="aba('filhos','fil')">Filhos</a>
                        </li>
                        <li id="mor">
                            <a href="#moradia" onclick="aba('moradia','mor')">Dados da Moradia</a>
                        </li>
                        <li id="arq">
                            <a href="#arquivo" onclick="aba('arquivo','arq')">Dados de Arquivo</a>
                        </li>
                    </ul>
                    <div class="cadastro" id="pessoais">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS PESSOAIS</td> </tr>
                            <tr><td>Nome:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="nome" value="<?php echo $resultado["nome"] ?>"> </td> </tr>
                            <tr><td>Filiação:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="filiacao" value="<?php echo $resultado["filiacao"] ?>"> </td> </tr>
                            <tr><td>Nascimento:</td><td><input type="text" size="30" maxlength="10" name="nascimento" value="<?php echo $resultado["nascimento"] ?>"> </td> <td>Estado Civil:</td> <td><?php echo $estado_civil -> listar_combo(); ?></td> </tr>
                            <tr><td>Naturalidade:</td><td><input type="text" size="30" maxlength="50" name="naturalidade"  value="<?php echo $resultado["naturalidade"] ?>"> </td><td>Nacionalidade:</td><td><input type="text" size="30" maxlength="20" name="nacionalidade"  value="<?php echo $resultado["nacionalidade"] ?>"> </td> </tr>
                            <tr><td>RG:</td><td><input type="text" size="30" maxlength="10" name="rg" value="<?php echo $resultado["rg"] ?>"></td><td>CPF:</td><td><input type="text" size="30" maxlength="11" name="cpf" value="<?php echo $resultado["cpf"] ?>"> </td> </tr>
			    <tr><td>Profissão:</td><td><input type="text" size="30" maxlength="20" name="profissao" value="<?php echo $resultado["profissao"] ?>"></td><td>Tempo de atividade:</td><td><input type="text" size="30" maxlength="50" name="tempo_atividade" value="<?php echo $resultado["tempo_atividade"] ?>"></td> </tr>
                            <tr><td>Empresa:</td><td colspan="3"><input type="text" size="80" maxlength="50" name="empresa" value="<?php echo $resultado["empresa"] ?>"> </td> </tr>
                            <tr><td>FGTS:</td><td><select name="fgts" id="fundo"><option value=""></option> <option value=1>Sim</option> <option value=0>Não</option></select> </td><td>PIS/PASEP:</td><td><input type="text" size="30" maxlength="20" name="pis_pasep" value="<?php echo $resultado["pis_pasep"] ?>"> </td> </tr>
                        </table>
                    </div>
                    <div class="cadastro" id="endereco_atual">
                        <table align="center">
                            <tr><td colspan="4" align="center">ENDEREÇO ATUAL</td></tr>
                            <tr><td>Endereço:</td><td><input type="text" size="30" maxlength="100" name="endereco"  value="<?php echo $resultado["endereco"] ?>"> </td><td>Bairro:</td><td><?php echo $bairros -> listar_combo(0); ?></td></tr>
                            <tr><td>Cidade:</td><td><input type="text" size="30" maxlength="50" name="cidade"  value="<?php echo $resultado["cidade"] ?>"> </td><td>Município de Origem:</td><td><input type="text" size="30" maxlength="50" name="municipio_origem" value="<?php echo $resultado["municipio_origem"] ?>"> </td></tr>
                            <tr><td>Tempo de Moradia:</td><td><input type="text" size="30" maxlength="10" name="tempo_moradia" value="<?php echo $resultado["tempo_moradia"] ?>"></td><td>Situação:</td><td><?php echo $moradia_situacao -> listar_combo(0); ?></td></tr>
                            <tr><td>Tipo de Construção:</td><td><?php echo $construcao_tipo["combo"]["ea"] ?></td><td>Luz:</td><td><?php echo $luz -> listar_combo(0); ?></td></tr>
                            <tr><td>Número de Cômodos:</td><td><input type="text" size="30" maxlength="10" name="ea_comodos" value="<?php echo $resultado["ea_comodos"] ?>"></td><td>Número de Moradores:</td><td><input type="text" size="30" maxlength="11" name="ea_moradores" value="<?php echo $resultado["ea_moradores"] ?>"> </td> </tr>
                            <tr><td>Água:</td><td><?php echo $agua -> listar_combo(0); ?></td><td>Risco:</td><td><select name="ea_risco" id="ea_risc"><option value=""></option> <option value="1">Sim</option> <option value="0">Não</option></select></td></tr>
                            <tr><td>Saneamento:</td><td><?php echo $esgoto -> listar_combo(0); ?></td><td><input id="ter" name="terreno" type="checkbox" value="1" onclick="on_off('terreno')">Possui Terreno:</td><td><input disabled type="text" size="30" maxlength="50" name="terreno_desc" id="terrenoo" value="<?php echo $resultado["terreno_desc"] ?>"></td></tr>
                            <tr><td>Local do Terreno</td><td><input type="text" size="30" maxlength="50" name="local_terreno" value="<?php echo $resultado["local_terreno"] ?>"></td><td>Tempo de Aluguel:</td><td><input type="text" size="30" maxlength="50" name="tempo_aluguel" value="<?php echo $resultado["tempo_aluguel"] ?>"></td></tr>
                        </table>
                    </div>
                    <div class="cadastro" id="complementares">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS COMPLEMENTARES</td> </tr>
                            <tr><td>Telefone:</td><td><input type="text" size="30" maxlength="20" name="telefone"  value="<?php echo $resultado["telefone"] ?>"></td><td>Celular:</td><td><input type="text" size="30" maxlength="20" name="celular"  value="<?php echo $resultado["celular"] ?>"> </td> </tr>
                            <tr><td>Email:</td><td><input type="text" size="30" maxlength="50" name="email"  value="<?php echo $resultado["email"] ?>"></td><td><input name="veiculo" id="carro" type="checkbox" onclick="on_off('veiculoo')" value="1">Veículo:</td><td><input disabled type="text" size="30" maxlength="50" name="veiculo_desc" id="veiculoo"  value="<?php echo $resultado["veiculo_desc"] ?>"> </td> </tr>
                            <tr><td>Escolaridade:</td><td><?php echo $escolaridade["combo"] ?></td><td><input name="necessidades" id="nec" type="checkbox" onclick="on_off('necessidadess')" value="1">Necessidades Especiais:</td><td><input disabled type="text" size="30" maxlength="50" name="necessidade_desc" id="necessidadess" value="<?php echo $resultado["necessidade_desc"] ?>"></td> </tr>
                            <tr><td>Renda:</td><td><input type="text" size="30" maxlength="20" name="renda"  value="<?php echo $resultado["renda"] ?>"></td><td>Renda Comprovada:</td><td><select name="renda_comprovada" id="rc"> <option value="1">Sim</option> <option value="0" selected>Não</option></select></tr>
                        </table>
                    </div>
                    <div class="cadastro" id="conjuge">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS DO(A) CONJUGE</td> </tr>
                            <tr><td>Nome:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="conjuge_nome"  value="<?php echo $resultado["conjuge_nome"] ?>"> </td> </tr>
                            <tr><td>Nascimento:</td><td><input type="text" size="30" maxlength="10" name="conjuge_nascimento"  value="<?php echo $resultado["conjuge_nascimento"] ?>"> </td><td>Profissão:</td><td><input type="text" size="30" maxlength="20" name="conjuge_profissao" value="<?php echo $resultado["conjuge_profissao"] ?>"></td></tr>
                            <tr><td>Local de Trabalho:</td><td><input type="text" size="30" maxlength="50" name="conjuge_local_trabalho" value="<?php echo $resultado["conjuge_local_trabalho"] ?>"> </td><td>Escolaridade:</td><td><?php echo $escolaridade["combo_conjuge"]; ?></td></tr>
                            <tr><td>Naturalidade:</td><td><input type="text" size="30" maxlength="50" name="conjuge_naturalidade" value="<?php echo $resultado["conjuge_naturalidade"] ?>"> </td><td>Nacionalidade:</td><td><input type="text" size="30" maxlength="20" name="conjuge_nacionalidade"  value="<?php echo $resultado["conjuge_nacionalidade"] ?>"> </td> </tr>
                            <tr><td>RG:</td><td><input type="text" size="30" maxlength="10" name="conjuge_rg" value="<?php echo $resultado["conjuge_rg"] ?>"></td><td>CPF:</td><td><input type="text" size="30" maxlength="11" name="conjuge_cpf" value="<?php echo $resultado["conjuge_cpf"] ?>"> </td> </tr>
                            <tr><td>Tipo de Relação:</td><td><input type="text" size="30" maxlength="50" name="conjuge_relacao" value="<?php echo $resultado["conjuge_relacao"] ?>"></td><td>Tempo de Relação:</td><td><input type="text" size="30" maxlength="11" name="tempo_relacao" value="<?php echo $resultado["tempo_relacao"] ?>"> </td> </tr>
                            <tr><td>Renda:</td><td><input type="text" size="30" maxlength="20" name="renda_conjuge"  value="<?php echo $resultado["renda_conjuge"] ?>"></td><td>Renda Comprovada:</td><td><select name="renda_conjuge_comprovada" id="rcc"> <option value="1">Sim</option> <option value="0" selected>Não</option></select></tr>
                        </table>
                    </div>
                    <div class="cadastro" id="filhos">
                        <table align="center">
                            <tr><td colspan="3" align="center">FILHOS</td> </tr>
                            <tr>
                                <td>Nome:</td>
                                <td><input type="text" id="filho_nome" size="18"> </td>
                                <td id="cadsis"><input type="button" value="Adicionar" onclick="cadfilho()"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <select id="combo_filhos" name="filhos[]" multiple size="3" style="width: 100%" onkeydown="apaga(window.event.keyCode);">

                                    </select>
                                </td>
                                <td style="vertical-align: top;"><input type="button" value="Remover" onclick="apaga(46)"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="cadastro" id="moradia">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS DA MORADIA</td> </tr>
                            <tr><td>Endereço:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="terreno_endereco"  value="<?php echo $resultado["terreno_endereco"] ?>"> </td> </tr>
                            <tr><td>Bairro:</td><td><?php echo $bairros -> listar_combo(1); ?></td><td>Tipo de Construção:</td><td><?php echo $construcao_tipo["combo"]["moradia"] ?></td></tr>
                            <tr><td>Número de Cômodos:</td><td><input type="text" size="30" maxlength="10" name="construcao_comodos"  value="<?php echo $resultado["construcao_comodos"] ?>"></td><td>Número de Moradores:</td><td><input type="text" size="30" maxlength="11" name="construcao_moradores"  value="<?php echo $resultado["construcao_moradores"] ?>"> </td> </tr>
                            <tr><td>Luz:</td><td><?php echo $luz -> listar_combo(1); ?></td><td>Água:</td><td><?php echo $agua -> listar_combo(1); ?></td> </tr>
                            <tr><td>Risco:</td><td><select name="moradia_risco" id="mr"><option value=""></option> <option value="1">Sim</option> <option value="0">Não</option></select></td><td>Saneamento:</td><td><?php echo $esgoto -> listar_combo(1); ?></td></tr>
                            <tr><td>Situação:</td><td colspan="3"><?php echo $moradia_situacao -> listar_combo(1); ?></td></tr>
                            <tr><td>Observação:</td><td colspan="3"><input type="text" size="80" maxlength="50" name="situacao_obs"  value="<?php echo $resultado["situacao_obs"] ?>"> </td> </tr>
                        </table>
                    </div>
                    <div class="cadastro" id="arquivo">
                        <table align="center">
                            <tr><td colspan="6" align="center">DADOS DE ARQUIVO</td> </tr>
                            <tr><td>Programa:</td><td colspan="5"><?php echo $programas -> listar_combo(); ?></td> </tr>
                            <tr><td>Situação:</td><td colspan="3"><input type="text" size="30" maxlength="70" name="situacao" value="<?php echo $resultado["situacao"]; ?>"></td><td>Interesse:</td><td><input type="text" size="30" maxlength="50" name="interesse" value="<?php echo $resultado["interesse"]; ?>"></td></tr>
                            <tr><td>Documentos Faltantes:</td><td colspan="5"><textarea name="documentos_faltantes" cols="75" rows="4"><?php echo $resultado["documentos_faltantes"];?></textarea> </td> </tr>
                            <tr><td>Documentos Arquivados:</td><td colspan="5"><textarea name="documentos_arquivados" cols="75" rows="4"><?php echo $resultado["documentos_arquivados"];?></textarea> </td> </tr>
                            <tr><td>Arquivado:</td><td><select name="arquivado" id="arquivadoo"><option value=""></option> <option value=1>Sim</option> <option value=0>Não</option></select> </td><td>Prateleira:</td><td><input type="text" size="30" maxlength="10" name="prateleira" value="<?php echo $resultado["prateleira"]; ?>"></td><td>Pasta:</td><td><input type="text" size="30" maxlength="50" name="pasta" value="<?php echo $resultado["pasta"]; ?>"></tr>
                            <tr><td>Observação:</td><td colspan="5"><input type="text" size="100" maxlength="255" name="observacoes" value="<?php echo $resultado["observacoes"]; ?>"> </td> </tr>
                        </table>
                    </div>
                    <div class="cadastro" style="display: block">
                        <table align="center">
                            <tr><td>Data do Cadastro:</td><td><input type="text" size="30" disabled name="data_cadastro" value="<?php echo $resultado["data_cadastro"]; ?>"></td><td>Código:</td><td><input type="text" name="codigo" size="5" value="<?php echo $resultado["codigo"]; ?>"></td><?php if($_SESSION["admin"]) echo '<td align="center"><input type="submit" value="Cadastrar" onclick="selecionaTodos();"></td>'; ?></tr>
                        </table>
                        <p style="margin: 0px; padding: 0px; text-align: center; margin-top: 10px; margin-bottom: 10px;"><a href="regras/PDFindividual.php?codigo=<?php echo $_GET["codigo"]; ?>">Exportar para PDF</a></p>
                    </div>
                    <input type="hidden" name="codigo" style="margin: 0px; padding: 0px; display: none;" value="<?php echo $_GET["codigo"]; ?>">
                </form>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
        <script>
        window.onload=function(){
            selecionaTudo();
        }
        </script>
    </body>
</html>