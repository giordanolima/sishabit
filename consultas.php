<?php
    include 'regras/config.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
    </head>
    <body>
            <?php
            echo $menu;
            ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>Consultas</H1>
                <div class="fleft" id="consultas" style="width: 250px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Relatório Geral</p>
                    <p style="margin: 15px;"><a href="listar_todos.php" >Listar todos os registros</a></p>
                </div>
                <div class="fleft" id="consultas" style="width: 270px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Buscar Registro por Código</p>
                    <table align="center" style="margin: 15px;">
                        <form action="busca_codigo.php" method="post" onsubmit="if(this.codigo.value==''){alert('Você deve digitar o código!'); this.codigo.focus(); return false} ">
                        <tr>
                            <td><input type="text" name="codigo" size="18"></td>
                            <td><input type="submit" value="Buscar"></td>
                        </tr>
                        </form>
                    </table>
                </div>

                <div class="fleft" id="consultas" style="width: 270px;">
                    <p id="cadsis" style="margin: 0px; margin-bottom: 10px;">Busca por Nome</p>
                    <table align="center" style="margin: 15px;">
                        <form action="busca_nome.php" method="post" onsubmit="if(this.nome.value==''){alert('Você deve digitar o nome!'); this.nome.focus(); return false} ">
                            <tr>
                            <td><input type="text" name="nome" size="18"></td>
                            <td><input type="submit" value="Buscar"></td>
                        </tr>
                        </form>
                    </table>
                </div>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
