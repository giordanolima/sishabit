<?php

    include 'regras/conexao.php';
    include 'regras/config.php';
    include 'regras/classListarEstadoCivil.php';
    include 'regras/classListarAgua.php';
    include 'regras/classListarLuz.php';
    include 'regras/classListarSaneamento.php';
    include 'regras/classListarSituacao.php';
    include 'regras/classListarPrograma.php';
    include 'regras/classListarBairros.php';

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SisHabit - Sistema de Controle Habitacional</title>
        <link rel="stylesheet" type="text/css" href="regras/estilo.css">
        <script type="text/javascript" src="regras/js/script.js"></script>
        <script type="text/javascript" src="regras/js/select_options.js"></script>
    </head>
    <body onload="aba('pessoais','pess')">
        <?php
        echo $menu;
        ?>
        <div class="tudo" style="background: lightgray;">
            <div class="centro" id="master">
                <H1>NOVO CADASTRO</H1>
                <form name="cadastro" action="regras/cadastrar_registro.php" method="post" onsubmit="return valida(this);">
                    <ul class="menu-cad" align="center">
                        <li id="pess">
                            <a href="#pessoais" onclick="aba('pessoais','pess')">Dados Pessoais</a>
                        </li>
                        <li id="end">
                            <a href="#endereco_atual" onclick="aba('endereco_atual','end')">Endereço Atual</a>
                        </li>
                        <li id="comp">
                            <a href="#complementares" onclick="aba('complementares','comp')">Dados Complementares</a>
                        </li>
                        <li id="conj">
                            <a href="#conjuge" onclick="aba('conjuge','conj')">Dados do(a) Conjuge</a>
                        </li>
                        <li id="fil">
                            <a href="#filhos" onclick="aba('filhos','fil')">Filhos</a>
                        </li>
                        <li id="mor">
                            <a href="#moradia" onclick="aba('moradia','mor')">Dados da Moradia</a>
                        </li>
                        <li id="arq">
                            <a href="#arquivo" onclick="aba('arquivo','arq')">Dados de Arquivo</a>
                        </li>
                    </ul>
                    <div class="cadastro" id="pessoais">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS PESSOAIS</td> </tr>
                            <tr><td>Nome:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="nome"> </td> </tr>
                            <tr><td>Filiação:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="filiacao"> </td> </tr>
                            <tr><td>Nascimento:</td><td><input type="text" size="30" maxlength="10" name="nascimento"> </td> <td>Estado Civil:</td> <td><?php echo $estado_civil -> listar_combo(); ?></td> </tr>
                            <tr><td>Naturalidade:</td><td><input type="text" size="30" maxlength="50" name="naturalidade"> </td><td>Nacionalidade:</td><td><input type="text" size="30" maxlength="20" name="nacionalidade" value="Brasil"> </td> </tr>
                            <tr><td>RG:</td><td><input type="text" size="30" maxlength="10" name="rg"></td><td>CPF:</td><td><input type="text" size="30" maxlength="11" name="cpf"> </td> </tr>
			    <tr><td>Profissão:</td><td><input type="text" size="30" maxlength="20" name="profissao"></td><td>Tempo de atividade:</td><td><input type="text" size="30" maxlength="50" name="tempo_atividade"></td> </tr>
                            <tr><td>Empresa:</td><td colspan="3"><input type="text" size="80" maxlength="50" name="empresa"> </td> </tr>
                            <tr><td>FGTS:</td><td><select name="fgts"><option value=""></option> <option value="1">Sim</option> <option value="0">Não</option></select> </td><td>PIS/PASEP:</td><td><input type="text" size="30" maxlength="20" name="pis_pasep"> </td> </tr>
                        </table>
                    </div>
                    <div class="cadastro" id="endereco_atual">
                        <table align="center">
                            <tr><td colspan="4" align="center">ENDEREÇO ATUAL</td></tr>
                            <tr><td>Endereço:</td><td><input type="text" size="30" maxlength="100" name="endereco"> </td><td>Bairro:</td><td><?php echo $bairros -> listar_combo(0); ?></td></tr>
                            <tr><td>Cidade:</td><td><input type="text" size="30" maxlength="50" name="cidade"> </td><td>Município de Origem:</td><td><input type="text" size="30" maxlength="50" name="municipio_origem"> </td></tr>
                            <tr><td>Tempo de Moradia:</td><td><input type="text" size="30" maxlength="10" name="tempo_moradia"></td><td>Situação:</td><td><?php echo $moradia_situacao -> listar_combo(0); ?></td></tr>
                            <tr><td>Tipo de Construção:</td><td><?php echo $construcao_tipo["combo"]["ea"] ?></td><td>Luz:</td><td><?php echo $luz -> listar_combo(0); ?></td></tr>
                            <tr><td>Número de Cômodos:</td><td><input type="text" size="30" maxlength="10" name="ea_comodos"></td><td>Número de Moradores:</td><td><input type="text" size="30" maxlength="11" name="ea_moradores"> </td> </tr>
                            <tr><td>Água:</td><td><?php echo $agua -> listar_combo(0); ?></td><td>Risco:</td><td><select name="ea_risco"><option value=""></option> <option value="1">Sim</option> <option value="0">Não</option></select></td></tr>
                            <tr><td>Saneamento:</td><td><?php echo $esgoto -> listar_combo(0); ?></td><td><input name="terreno" type="checkbox" value="1" onclick="on_off('terreno')">Possui Terreno:</td><td><input disabled type="text" size="30" maxlength="50" name="terreno_desc" id="terreno"></td></tr>
                            <tr><td>Local do Terreno</td><td><input type="text" size="30" maxlength="50" name="local_terreno"></td><td>Tempo de Aluguel:</td><td><input type="text" size="30" maxlength="50" name="tempo_aluguel"></td></tr>
                        </table>
                    </div>
                    <div class="cadastro" id="complementares">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS COMPLEMENTARES</td> </tr>
                            <tr><td>Telefone:</td><td><input type="text" size="30" maxlength="20" name="telefone"></td><td>Celular:</td><td><input type="text" size="30" maxlength="20" name="celular"> </td> </tr>
                            <tr><td>Email:</td><td><input type="text" size="30" maxlength="50" name="email"></td><td><input name="veiculo" type="checkbox" value="1" onclick="on_off('veiculo')">Veículo:</td><td><input disabled type="text" size="30" maxlength="50" name="veiculo_desc" id="veiculo"> </td> </tr>
                            <tr><td>Escolaridade:</td><td><?php echo $escolaridade["combo"] ?></td><td><input name="necessidades" type="checkbox" value="1" onclick="on_off('necessidades')">Necessidades Especiais:</td><td><input disabled type="text" size="30" maxlength="50" name="necessidade_desc" id="necessidades"></td> </tr>
                            <tr><td>Renda:</td><td><input type="text" size="30" maxlength="20" name="renda"></td><td>Renda Comprovada:</td><td><select name="renda_comprovada"> <option value="1">Sim</option> <option value="0" selected>Não</option></select></tr>
                        </table>
                    </div>
                    <div class="cadastro" id="conjuge">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS DO(A) CONJUGE</td> </tr>
                            <tr><td>Nome:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="conjuge_nome"> </td> </tr>
                            <tr><td>Nascimento:</td><td><input type="text" size="30" maxlength="10" name="conjuge_nascimento"> </td><td>Profissão:</td><td><input type="text" size="30" maxlength="20" name="conjuge_profissao"></td></tr>
                            <tr><td>Local de Trabalho:</td><td><input type="text" size="30" maxlength="50" name="conjuge_local_trabalho"> </td><td>Escolaridade:</td><td><?php echo $escolaridade["combo_conjuge"]; ?></td></tr>
                            <tr><td>Naturalidade:</td><td><input type="text" size="30" maxlength="50" name="conjuge_naturalidade"> </td><td>Nacionalidade:</td><td><input type="text" size="30" maxlength="20" name="conjuge_nacionalidade" value="Brasil"> </td> </tr>
                            <tr><td>RG:</td><td><input type="text" size="30" maxlength="10" name="conjuge_rg"></td><td>CPF:</td><td><input type="text" size="30" maxlength="11" name="conjuge_cpf"> </td> </tr>
                            <tr><td>Tipo de Relação:</td><td><input type="text" size="30" maxlength="50" name="conjuge_relacao"></td><td>Tempo de Relação:</td><td><input type="text" size="30" maxlength="11" name="tempo_relacao"> </td> </tr>
                            <tr><td>Renda:</td><td><input type="text" size="30" maxlength="20" name="renda_conjuge"></td><td>Renda Comprovada:</td><td><select name="renda_conjuge_comprovada"> <option value="1">Sim</option> <option value="0" selected>Não</option></select></tr>
                        </table>
                    </div>
                    <div class="cadastro" id="filhos">
                        <table align="center">
                            <tr><td colspan="3" align="center">FILHOS</td> </tr>
                            <tr>
                                <td>Nome:</td>
                                <td><input type="text" id="filho_nome" size="18"> </td>
                                <td id="cadsis"><input type="button" value="Adicionar" onclick="cadfilho()"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <select multiple name="filhos[]" id="combo_filhos" size="3" style="width: 100%" onkeydown="apaga(window.event.keyCode);">

                                    </select>
                                </td>
                                <td style="vertical-align: top;"><input type="button" value="Remover" onclick="apaga(46)"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="cadastro" id="moradia">
                        <table align="center">
                            <tr><td colspan="4" align="center">DADOS DA MORADIA</td> </tr>
                            <tr><td>Endereço:</td><td colspan="3"><input type="text" size="80" maxlength="100" name="terreno_endereco"> </td> </tr>
                            <tr><td>Bairro:</td><td><?php echo $bairros -> listar_combo(1); ?></td><td>Tipo de Construção:</td><td><?php echo $construcao_tipo["combo"]["moradia"] ?></td></tr>
                            <tr><td>Número de Cômodos:</td><td><input type="text" size="30" maxlength="10" name="construcao_comodos"></td><td>Número de Moradores:</td><td><input type="text" size="30" maxlength="11" name="construcao_moradores"> </td> </tr>
                            <tr><td>Luz:</td><td><?php echo $luz -> listar_combo(1); ?></td><td>Água:</td><td><?php echo $agua -> listar_combo(1); ?></td> </tr>
                            <tr><td>Risco:</td><td><select name="moradia_risco"><option value=""></option> <option value="1">Sim</option> <option value="0">Não</option></select></td><td>Saneamento:</td><td><?php echo $esgoto -> listar_combo(1); ?></td></tr>
                            <tr><td>Situação:</td><td colspan="3"><?php echo $moradia_situacao -> listar_combo(1); ?></td></tr>
                            <tr><td>Observação:</td><td colspan="3"><input type="text" size="80" maxlength="50" name="situacao_obs"> </td> </tr>
                        </table>
                    </div>
                    <div class="cadastro" id="arquivo">
                        <table align="center">
                            <tr><td colspan="6" align="center">DADOS DE ARQUIVO</td> </tr>
                            <tr><td>Programa:</td><td colspan="5"><?php echo $programas -> listar_combo(); ?></td> </tr>
                            <tr><td>Situação:</td><td colspan="3"><input type="text" size="30" maxlength="70" name="situacao"></td><td>Interesse:</td><td><input type="text" size="30" maxlength="50" name="interesse"></td></tr>
                            <tr><td>Documentos Faltantes:</td><td colspan="5"><textarea name="documentos_faltantes" cols="75" rows="4"></textarea> </td> </tr>
                            <tr><td>Documentos Arquivados:</td><td colspan="5"><textarea name="documentos_arquivados" cols="75" rows="4"></textarea> </td> </tr>
                            <tr><td>Arquivado:</td><td><select name="arquivado" id="arquivadoo"><option value=""></option> <option value=1>Sim</option> <option value=0>Não</option></select> </td><td>Prateleira:</td><td><input type="text" size="30" maxlength="10" name="prateleira"></td><td>Pasta:</td><td><input type="text" size="30" maxlength="50" name="pasta"></tr>
                            <tr><td>Observação:</td><td colspan="5"><input type="text" size="100" maxlength="255" name="observacoes"> </td> </tr>
                        </table>
                    </div>
                    <div class="cadastro" style="display: block">
                        <table align="center">
                            <tr><td style="padding: 3px;">Data do Cadastro:</td><td style="padding: 3px;"><?php echo date("d/m/Y"); ?><input type="hidden" size="30" name="data_cadastro" value="<?php echo date("d/m/Y"); ?>"></td><td>Código:</td><td><input type="text" name="codigo" size="5"></td><td align="center" style="padding: 3px;"><input type="submit" value="Cadastrar" onclick="selecionaTodos();"></td></tr>
                        </table>
                    </div>
                    <input type="hidden" name="data_cadastro" value="<?php echo date("d/m/Y"); ?>">
                </form>
                <h4>Desenvolvido pela <a href="http://www.palmi.com.br" target="_blank">PALMI Informática</a>. Todos os direitos reservados</h4>
            </div>
        </div>
    </body>
</html>
